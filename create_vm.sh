#!/bin/bash

# x86_64 or x86
ARCH=x86_64
BUILD=1

DISK=vm_$ARCH.img
DEFAULT_USER=dev
DEFAULT_PASSWORD=uiuc
ROOT_PASSWORD=uiuc
PACKAGES=(# Core packages
          base
          base-devel
          linux-lts{,-headers} # use lts because current VirtualBox (~5.1.2) doesn't play nicely with the new (4.7) kernel
          grub
          # X things
          virtualbox-guest-dkms
          virtualbox-guest-utils
          xfce4
          sfml
          # Nice-to-haves
          bash-completion
          #zsh # if they want zsh, they can install it themselves
          htop
          # VCS
          subversion
          #git
          # Editors
          gvim
          gedit
          emacs
          # Random utilities
          sudo
          imagemagick
          ristretto
          graphviz
          openssh
          gnuplot
          gtkwave # cs233
          qt4 # cs233 (qtspim)
          unzip
          python2-matplotlib # for lab_btree
          # Browsers
          chromium
          #firefox # removed due to size (+ dependencies)
          # Development (compilers, interpreters, debuggers, etc.)
          clang35
          llvm35
          libc++
          gdb
          valgrind
          iverilog # cs233
)
IGNORE_PACKAGES=(
                 linux # using linux-lts
                 netctl dhcpcd # using systemd-networkd
)


set -e -x

m() {
    echo "$@"
}


if [[ -n "$BUILD" ]]; then
rm -f $DISK

m "Creating disk image..."
truncate $DISK --size=8G

m "Creating partition table..."
fdisk $DISK <<fdiskEOF
o
n




a
w
fdiskEOF
fi # -n $BUILD

m "Mounting virtual drive... (requires sudo)"

loopdev=$(sudo losetup -f)

sudo losetup --partscan "$loopdev" $DISK

trap "sudo losetup -d $loopdev" EXIT

if [[ -n "$BUILD" ]]; then
m "Creating filesystem... (requires sudo)"
sudo mkfs.ext3 ${loopdev}p1
fi # -n $BUILD

mkdir -p root

m "Mounting filesystem... (requires sudo)"
sudo mount ${loopdev}p1 root/

trap "sudo umount ${loopdev}p1 ; sudo losetup -d $loopdev" EXIT

if [[ -n "$BUILD" ]]; then
if [[ $ARCH == "x86_64" ]]; then
    sudo pacstrap -C pacman-x86_64.conf -c -G root/ "${PACKAGES[@]}"
else
    sudo linux32 pacstrap -C pacman-x86.conf -c -G root/ "${PACKAGES[@]}" --arch=i686
fi

sudo arch-chroot root/ <<chrootEOF
pacman -Rs --noconfirm ${IGNORE_PACKAGES[@]}

# random setup - hostname, timezone, locales
echo cs2xx-vm > /etc/hostname
ln -s /usr/share/zoneinfo/America/Chicago /etc/localtime
sed -i '/^#en_US/ s/^#//' /etc/locale.gen
locale-gen
echo LANG=en_US.UTF-8 > /etc/locale.conf
/usr/bin/mandb


# networking
ln -s /dev/null /etc/udev/rules.d/80-net-setup-link.rules
ln -sf /usr/lib/systemd/resolv.conf /etc/resolv.conf
sed -i '/^hosts:/ s/\<dns\>/resolve/' /etc/nsswitch.conf
systemctl enable systemd-networkd systemd-resolved.service
cat > /etc/systemd/network/wired.network <<"networkEOF"
[Match]
Name=eth0

[Network]
DHCP=ipv4
networkEOF


# setup pacman keyring
pacman-key --init
pacman-key --populate archlinux

# Rebuild dynamic link cache
ldconfig -X

# install bootloader
sed -i '/TIMEOUT/ s+GRUB_TIMEOUT=.*+GRUB_TIMEOUT=2+' /etc/default/grub
grub-install --target=i386-pc ${loopdev}
grub-mkconfig -o /boot/grub/grub.cfg


# fix a bug when running in QEMU - the 'block' hook has to be included in the initcpio
sed -i '/^HOOKS/ s/autodetect \\(.*\\) block/block autodetect \\1/' /etc/mkinitcpio.conf
mkinitcpio -p linux-lts


# create default user, set passwords
useradd -G wheel -m $DEFAULT_USER
echo "$DEFAULT_USER:$DEFAULT_PASSWORD" | chpasswd
echo "root:$ROOT_PASSWORD" | chpasswd
echo "%wheel ALL=(ALL) ALL" > /etc/sudoers.d/wheel


# autologin on tty1
mkdir -p /etc/systemd/system/getty@tty1.service.d
cat > /etc/systemd/system/getty@tty1.service.d/override.conf <<"gettyEOF"
[Service]
ExecStart=
ExecStart=-/usr/bin/agetty --autologin $DEFAULT_USER --noclear %I 38400 linux
gettyEOF


# autostart X (xfce) on tty1
echo '[[ -z \$DISPLAY && \$XDG_VTNR -eq 1 && -x /usr/bin/startx ]] && exec startx' >> /home/$DEFAULT_USER/.bashrc
chown $DEFAULT_USER:$DEFAULT_USER /home/$DEFAULT_USER/.bashrc
cat > /home/$DEFAULT_USER/.xinitrc <<"xinitrcEOF"
#!/bin/sh
#
# ~/.xinitrc
#
# Executed by startx (run your window manager from here)

if [ -d /etc/X11/xinit/xinitrc.d ]; then
  for f in /etc/X11/xinit/xinitrc.d/*; do
    [ -x "\$f" ] && . "\$f"
  done
  unset f
fi
/usr/bin/VBoxClient-all
exec startxfce4
xinitrcEOF
chown $DEFAULT_USER:$DEFAULT_USER /home/$DEFAULT_USER/.xinitrc


# XFCE config
# generate with:
#     tar Pcvz /home/dev/.config/xfce4/ | base64
base64 --decode <<xfceConfigEOF | tar Pzvx
H4sIAGpsmVYAA+0daY/bNjaf/SuEAN0vhca6LE/quos0M9MGbQ40k0yxRWHQEm1rI4mCjrFd7I9f
HpIty6JEyh53s9FDm7EkvsfjnXwkpeEKBXDowsfhlYPChbccbhYOtIbPzggahvFoRP7q45FW/lvA
M92wLRv/b+Lfmm6ahvZMGZ2zETzIkhTEivIMD0FjubbnXygMOfzfLNbBuaSA8l/ThPhvjA3Mf8u0
zZ7/lwAe/12YfE5RdBYJkOD/aDweY/6PNM3o+X8JaOO/h+8mV4kTQxhq6th6sXlhmFexI1MHYbBt
WVz+65ZR4r/+TDOwu9CfKdpTdboMXzn//9gsclarjzBOPBSq1pWuXZnfqrGz8Hw4W6A4AOmfg/z2
NI0zOBj8MfxzEKP1VBs4yMf/4huFJLEHevHg7+5hD00gp/+6Zl7LGwAJ/bcs28b6r+P/ev2/BPT6
/3WDpP7rpvWk+s/iP6L/Vq//l4Be/79u6OD/dU2TMwAy/t8YW1T/7V7/LwK9/n/dwNP/CITQP2f+
TzD/a48skv+zzD7/dxFo5r8PstBZwRgb/hNkQSb/a9km5r9tmXbP/0uAOP91a2Rcv7BGmj26yl2G
YB1t/n+ElX3H/5FB/L9hGL3/vwT8ccNYqdyGabz9c/AWBHD6ahWjwMuCwU8whLHn0JsPcK78iD17
AuPBKxQEMEynLx0HJomSrqDyOkxhHMJ0cLuBztTJKSjffBzcwzjwQuBPF8BP4OA1lrPd88H9NoLT
l1Hkew5IcfQxeAVSuESxB5PpT/e/TN7CdI3izxNce175ZPDGCyDFS+EmHa7SwJ/QXxv8A+xJDTfk
0beb4nGAf2zUBMtzANUVCF0fxhg7jervJpPB7+rvd69u1Q8oix04JdHQd8PhMEviYbICMRyWKkuG
RZcK5fgyAh9R/T9lLVDG/tv6mNp/q/f/FwFx/uf239bG1nntP1ntPeQ/tv/mqLf/l4Ba+7+Erpce
GP97bECVW3wX7Y0/uVSIZVWIZUyY4aeoNVb/yM5/wMOeZtFblHqLLZtUHtr1yAdeOGHuAlA/Q72C
Sh6qMG9KyVm8fffmdkJcxsfU8710OyFtZk0mlpw+V2+Q8x6kK9bMIf33CjuI3fO7zPf3Q6CUu10U
+TFb/uX5Ptj9YFUfP38fIzdz0nw0jx7jYYxQSAZySQYa+MdFPrEZ+dS80q+vjOPnt5s0Bq/DBfrg
xF6UTqlr8r35sNQ9dZ4tYxihOL1KVoOXDnVW0xCu1bUXumg9IT9d5GSEp5PBL3CL/a2bUI5PiuG7
+TFLCOojSMHch4xdUt4RxcurZYhtzRUb9MJJDnYCyJqm7FuWC+O7CIYKUN7CtfJA75cFTVX35etp
FV07pnaTPzmmV+B8GS78JBC1//qF/P/IMNj8r/f/FwFx/hfzP+yiz+z/Sc73gP9k/tfnfy8Ctf7/
zovhAm1ap3/smk7/HlDsu9hAu/gnnDOTumBklG+y1miAevm8fNmrn2f6V777GLpXAaL+82qTnTI9
PLq9wGXr4prC6WKncw/meEq7Zo6M/HofE6cK8xtyPjUfL74vZRWWPR/2bgp24Yf8oT4P31XAHGXp
d/iKFKmj9nDkmAnBdcktH9Dk+eVqv3OCxClH7H49zfxhQffv1p3/BxC1//al5v80/0/ywL3/vwSI
87+Y/5tnz/9aY+OQ/9j/W33+9yJQ6//vV1mIB+UOux/lDQjBst7n02n/NklhgM11utrdU4Icpxw/
HBCjZj1ltXxzx7w/q7QaKdS406aU8QfanF0K4BWKIc0JkNrvEfIT+itvhqS7ZQ38stK7rSCo/y9O
WQqWsf/s/I9tYjPQ2/8LgDD/c/NvYd5Jmv/29T/dOOS/oev9/O8yULX/RcpRv9KODS0123CDVERC
f1Vl4qEUJvs2yHxAUqXUnmfUBrOMLXtea8wPzX3JlBc2nNlo8gcLpnqPIh8+Qn8yeBf62w8rtH4d
TmmB3HXl9JRdYwrH9TH3Wg6+xhM2xfdCyRwm6XnRFxXm9L9wfyAa/xmXyv+ZLP43x739vwSI87/I
/41s48zx/9iwD/lP1v/M3v5fAmrj/9sAOIn4+l8lIwfDpe8lq0lpFY/l59QAfIbExhbXzrffrty4
dJXEzu6q/KR0/9/gEezood3tCCQOKBKBaursf8LNYVZQTVbQ9xO6YraroNSGSe7myBjsJif06tgh
8r3XDXFSKKKLauWFyNwFPrx55YMkyUe6ftFNzjcRQh18kaj+m5ey/7bW2/8Lgjj/C/s/ts1z23/d
OuR/b/8vBrX2/6dPXiC9/eM+3rKF9EeMvPulqAtiRIXWfyhmzQygxn7yt4188d5HyuyTITtpBsJ/
/we5PM8BAKrjgvv/8/d/2Hq//n8RaOE/uVAjGDsrEGKHoG6CDodCJPz/SLNM8v6Hfv//haAD/1ne
nmybE6yjff3H2vOfzv+MERaJ3v9fAL7/J+ajkh/9mz7Xr7TnCgwd5Hrhcvr84/2dev38nz8MBt/n
EqCEJB54zmTg+SHiDwNF+T6KEZaXdJsXxLOcVH304Pq5khIf/TxJY0waYwI/w5dsUYW4/0+k0JBL
gxxEpYTUvxAKVJoE5NH8+ePbl7/N/vXu3ZvZr7efbn+dvX3325uXv9ZTD7zEUROM70PVwZ71c0F1
jpC/o0njFkLg+2E+Ej98oRm/Q+ig//TVYBLq36L/OjntW8r/k/3fhmH17/+4CHTUfyoDAuqfb20u
VAoGUbqlBY+LAra/GM4A3STE0e05vRrWk5ij2IUxnowkswBsvMD7C9YqM5k28GlsZgF6rEfcWQEe
ZgwTXqXNuFmaonDmgy3KUk7X3/3nw89vXrVQQItFAncUvDDdoWstmEkEHFqdBCq1l7MUzRbIyRLp
sXa2jo/5HUXJDIX+Vn7YGAE3BuvZIsZ3OrZg5bkuDDsi43mtF2RBR+wohmXvKImdgvnaC7G8ulCO
bxSb7KwkXIf1nGsceRdlc0yCCUCjxu40UYSS62FzHDq1vRkJUUi9oBYbh5Q8fAiS7ezA9Ve68NJP
ebhU8GcuxJorWSvDXNHSksxnqGEHsaFaMkNE1dPaBusav8GZ78/Wnpuu8CCnvryyrVDs/TVLnBhh
QpU2iEueF1JP0bUXhTi6XUxlgLIErlcQ+jPSiSySHgPiWjq2PPKxqpLU24G+V0TVgeQcbjuJmCSQ
aiWWXz+Ksqhr27GZI9UyyU1SCPySrxFnfgy8BHZSN4aJ/Rw/xm/i2w6d7+kEGk62yc1AuJ0xnyvf
ChhBkM6yeEkGc47HsGW2Uk+ExCgdGZlrb4vraOpDsgIuWhMepmC2gt5yJaeDB/jUHHVH33RHrR04
1WzBbRh0vugmK4yJo6QZmQPL85uiuwj7RtaIDhxDeXx1EgVmPxooNPciBBENr5N6f9mOjMNUNkGQ
b36Ozs4ddNB+SoArqzqf89vQIRU/zn3QRdVTcj6F4ylu4AJk/MAmJe9+QiF/LtQ0YDQ+mGEbvwyJ
t+nkqxiNBeKifwBhovyIfFd50UyDxh2YDvC7+HxKo31O2cwH2o7cBrDwhdMpcTJFHNSdEJ69p56z
G5VKw8QH54hQtWlipNByeeKkJAtj6HoxdNIZltoYBwny7uk034rDw5lDDnYnXsoLcJpq3/UeU8nC
WkG12nHJ9a7nII5BkXPBSJRKvcg8FAQUvahDFMGQRTBlEfbdHhb95gxDjG0tneDKjz5BPczByOEy
/8SbwPLtPEVu8i6tNZ+iNiShPssXkVtqLw/+/1km/OuETvl/XED9DLdzBGJXxbFdnDpZmnCXBJrz
/4Zp2+NK/t/WRv37Xy4CnfP/9TIgsCSQ77/f2ammNQE3j1Frih4X/oefTl766T+W6eROP0QZiuAY
9dXUROVsD5Ia0k1IvJoqbqq5bvNvqPt97AUg3pL6S025wbFuCiWGr56ML0jh97tr+8ZLolJCR6DO
Dxm+Q6qJujX0NnFAJNpJ0sSHhweJ0m+AV9//ttjFwdYIBfLibnImAUxNQRQtcGyBp1qycsWNQWTl
W29sIM0MqOWNbXjWmElqbXPXFVV1kO+DKIHu5YdBRNWO2u8j57N1ku7JkqzKbgX/+OQXKfwbBO5+
Fi+k4bWsclkZFU8gydQpwbXQpTXgC6tma3v3rxGRtS9na3KjJTqqJY/KCeFGjSDT3dhzBaY8B/Ja
uaoQzRf5n85Nvw4xJ1JBwypls3+FC1G6v5Uz0C1lP4r6mxu0DiVcUz4e92Auj0TldeUtulM4k8uX
7HM9EQnGHXS8ROI9WMJZNwbcWR1w7A444w441x1wXnTA0bUuSF0ibt3oLi91nJcUnnoJvA3dk2n8
jIIT9KmuazJWSpCksDHj9/OX9zMZvnOJnCAHeyLmOYjIGAAukdE5iMiYFS4RGTvDJSJjeLhEOlgi
ms88hzM4g6SfqH85ldMti1RXdmGsXFhQqlkqkVLG66jR1USIMF5Hvb3rqKpSTr+M11EhpZx/GU9G
7Q74LhMEHCB2lRhOMHDOlMlH3nQui2af4cmWrbrxkWz1Yos6++WZ2YsTarqWqum6uaZysFSh5+NH
JzTTlmqm3V5T7XSxQhy4bomoKMmSYaxuTmBbZNmanDA93mSsljjZExcnEpW0u3mhER93renACVaq
iskzGQ7IBr0NXWN1d+yUy6mAbZti+ZeutJsMTrehuuOx+gzsLc9aGkab7OXsoGp7F11VBh/tlOAs
YtNg2cr9ELNyR7P8qpikZNe5aOuP59ic3fLyFHmJcDAnfRamw/NmK8/tzKSaBFGFOqYadpEpnkfM
97iKtrcuUK7yee2lzkqIYmNO1yG7Q/yuQ1hKTjRIdgg3XU0xXzL39HRNkiRv9adEUpeQdB45ckCB
fan0LGaEa7zLQ90WOba7zEb0NpURHjSRCKFdTUSyrdVYEq1h3MVw8C1aSWwMSZLtFLtqZls43VEd
28dAcgh4XnhP0JIjOGolOJIjyPO0XaYMBymkCj22H52sq8m1j+dzhGde3Dk6l+Apk0aezNfPDcTt
cOPqtQt9GYkXyNxVbWd+nkberPD2Suyba54w2jx9rR9tOd3lpLuFajqlTzyTUV+TnPngJM6Famqx
K0+yJo4vHzHNuHZ7ddNOZraYPmwvuNsfd5ENt533f9IXxIm9BaLl/S9ju/T+57FlP9MM09TH/f7P
S8BJ+z+pDAht+SSS9bg/eXVwZjMX9KqiEdrtWnawv75WvWrq4tSmVlLXPMPCTndwD7xHU3uymWqT
7VTjGycfhsv9abCMc/qSX7dKtnJBt9W2HVMov6DioF6zqVo/W3qh6rmcYyUiXGksZQqV0kdCxSyh
UmK0bKFS5fMv1QWLlgUMJnqczcfSoqdr7bJ3ugydIA5jofG8Fir1QkxoxFioa2LFxORZFxRoQ6yY
xZevY+7QY5Vq5aixuInxIXiE3dHnwPm8jFEWuirwoxVooyAVfVG5Ezo6kIsob/bD22zMIcNNrrJP
u7Vg80L1FCSffXKguRld54XFCYxATD5CwLUXcIPNSycvkW79ejfBYRy39bzZQ0Q+wdPSc17HHWK5
WnC5CfstvgG2Ldi8fBlZFSqOyjVT4KUJ9kxrxudlBYp3MnN57qUwELPLFco1n3g5wa3lsstLJz95
N0ba/kNlp3eDZ0eevhul762e3g2eHXv6bpQ+G3F6N3gG9em7UXr7+end4JnGJ++GrY2t83WDZ2mf
vhumiIrXODdsxFUfzPdvEOUflj7Gdr2EfARdTTFO6kUiB665Ic7ffoa6c/4nH3aRDFBL/sewx9Xz
v6Y97t//exE4Kf+ze2dAawaITArcGEUikTtbwtXqih4XDlBIPgzw6aeXKgfjGGeXQeZi1KWwfBSr
B1FxbVDMMXQBDnb52KNmbPb+Y0KCY2hLXwfYz74SqqhMg8n71q7+HS0PJptHZrJhpDiZsX6kjkaK
e4q8H6nKSHHPvH+dI9UcfT1V0NDZ/xdnXtX8Y78NgUDr+/8Nq/L9h5E16t//fRE4yf9XZUAgECDa
JBIEsMVelfvOPNviv0yJofJfLTkqko//U5F4Dz300EMPPfTQQw899NBDDz300EMPPfTQQw899NBD
Dz2cDv8FkLba5QDIAAA=
xfceConfigEOF

chrootEOF

fi # -n $BUILD

# Open a root shell
#sudo arch-chroot root/

sudo umount ${loopdev}p1
sudo losetup -d $loopdev

trap - EXIT
set -e

rm -f $DISK.vmdk
VBoxManage convertfromraw $DISK $DISK.vmdk --format VMDK

uuid=$(uuidgen)

ostype=ArchLinux
[[ $ARCH == "x86_64" ]] && ostype=${ostype}_64

vmName="CS-2xx-$ARCH"

if VBoxManage list vms | grep -q -F "\"$vmName\""; then
    VBoxManage storagectl "$vmName" \
        --name IDE \
        --remove
    VBoxManage unregistervm "$vmName" --delete
fi

VBoxManage createvm --name "$vmName" --register --ostype $ostype --uuid "$uuid"
VBoxManage modifyvm "$uuid" \
    --description "CS 2xx Course Virtual Machine" \
    --memory 512 \
    --vram 12 \
    --biosbootmenu disabled \
    --rtcuseutc on \
    --nic1 nat \
    --cableconnected1 on \
    --clipboard bidirectional \
    --draganddrop bidirectional

VBoxManage storagectl "$uuid" \
    --name IDE \
    --add ide \
    --bootable on

VBoxManage closemedium "$(realpath $DISK.vmdk)" 

VBoxManage storageattach "$uuid" \
    --storagectl IDE \
    --port 0 \
    --device 0 \
    --type hdd \
    --medium "$(realpath $DISK.vmdk)" \
    --setuuid ""

rm -f $ARCH.ova
VBoxManage export "$uuid" \
    --output $ARCH.ova \
    --options nomacs
