## CS 2xx Course VM

### Requirements

For now, this script needs to be run on a 64-bit Arch Linux host, with
`extra/arch-install-scripts` and `community/virtualbox` installed.

If you don't need to create a VirtualBox VM and just want the raw disk image
(runnable with, e.g., QEMU), you can comment out the bottom of the script. 

If you do want the VirtualBox VM, it's better if VirtualBox is not running.


### Building

To build, edit the top of `create_vm.sh` and change `ARCH` to either `x86_64`
or `x86`. Then run the script.
